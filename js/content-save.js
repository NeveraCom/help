const contentSave = {
	copyTextareaBtn: document.getElementById('copy-result'),
	itemCategory: document.getElementById('category-select'),
  oldItem: document.getElementById('item-select'),
  newItem: document.getElementById('item-add'),
  newItemCaption: document.getElementById('item-add-caption'),
  itemLabels: document.getElementById('item-labels'),
  radioList: document.querySelectorAll('input[name=status]'),

  getStatus() {
  	return Array.from(this.radioList).find(e => e.checked)?.value;
  },

  setStatus(status = 'empty') {
  	document.querySelector(`input[name=status][value=${status}]`)?.setAttribute('checked', true);
  },

  /**
   * remove item from db
   */
  removeItemFromDb() {
  	if (this.oldItem.value) {
  		item.remove(this.oldItem.value)
  	}
  },


  /**
   * add new item to db / update old item
   */
  addItemToDb() {
  	const indexItemInSelect = Array.from(this.oldItem.options).findIndex(e => e.value === this.oldItem.value);
  	const isNewItem = this.newItem.value && this.newItemCaption.value;

	  const result = {
	    categoryId: this.itemCategory.value,
	    id: this.newItem.value || this.oldItem.value,
	    caption: this.newItemCaption.value || Array.apply(null, this.oldItem.options)[indexItemInSelect].text,
	    labels: this.itemLabels.value.length ? this.itemLabels.value.split(',').map(e => e.trim()) : [],
	    status: this.getStatus() || 'empty',
	    values: []
	  };

	  // get values from textarea
	  editor.save().then(res => {
    	result.values = res.blocks;
    });


	  // add item to library
	  const items = libraries.find(l => l.id === libs.current).items
	  if (isNewItem) {
	  	items.push(result)
	  } else {
	  	const index = items.findIndex(i => i.id === this.oldItem.value);
	  	items.splice(index, 1, result);
	  }

	  // update menu with new item
	  menu.displayLibsMenu();

	  // show toast
	  showToast('Item added to db', 'success');
  },


  /**
  	*	copy lib
  	*/
  copyLibToBuffer() {
  	if (!libs.current) return;

  	let data = '';

  	// add library categories
  	data += `libraries.find(e => e.id === '${libs.current}').categories.push(\n`;
  	
		for (let category of libraries.find(l => l.id === libs.current).categories) {
			data += `	{ id: '${category.id}', caption: '${category.caption}' },\n`
		}

  	data +=	`);\n\n`;

		// add library items
		data += `libraries.find(e => e.id === '${libs.current}').items.push(\n`;

		for (let item of libraries.find(l => l.id === libs.current).items) {
			data += '	' + JSON.stringify(item) + ',\n';
		}
		
		data += `);`;


  	const copyTextarea = document.getElementById('result');
    copyTextarea.innerText = data;
    console.log(data)


    copyTextarea.focus();
    copyTextarea.select();

    try {
      const successful = document.execCommand('copy');
      successful
        ? showToast('Data copied to clipboard', 'success')
        : showToast('Data not copied to clipboard', 'error');
    } catch (err) {
      showToast('Error in copy command', 'error');
    }
  },

  

  

//   editor.save().then(res => {
//     result.values = res.blocks;

//     const copyTextarea = document.getElementById('result');
//     copyTextarea.innerText = JSON.stringify(result);
//     copyTextarea.focus();
//     copyTextarea.select();

//     try {
//       const successful = document.execCommand('copy');
//       successful
//         ? showToast('Data copied to clipboard', 'success')
//         : showToast('Data not copied to clipboard', 'error');
//     } catch (err) {
//       showToast('Error in copy command', 'error');
//     }
//   });
// });
}
