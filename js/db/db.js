const libraries = [
  {
    id: 'html',
    caption: 'html',
    icon: 'html.svg',
    categories: [],
    items: [],
    block: 'html'
  },
  {
    id: 'markup',
    caption: 'markup',
    icon: 'markup.svg',
    categories: [],
    items: [],
    block: 'html'
  },
  {
    id: 'css',
    caption: 'css',
    icon: 'css.svg',
    categories: [],
    items: [],
    block: 'style'
  },
  {
    id: 'sass',
    caption: 'Sass',
    icon: 'sass.svg',
    categories: [],
    items: [],
    block: 'style'
  },
  {
    id: 'js',
    caption: 'Javascript',
    icon: 'javascript.svg',
    categories: [],
    items: [],
    block: 'lang'
  },
  {
    id: 'ts',
    caption: 'Typescript',
    icon: 'typescript.svg',
    categories: [],
    items: [],
    block: 'lang'
  },
  {
    id: 'angular',
    caption: 'Angular',
    icon: 'angular.svg',
    categories: [],
    items: [],
    block: 'angular'
  },
  {
    id: 'angular-material',
    caption: 'Angular Material',
    icon: 'angular-material.svg',
    categories: [],
    items: [],
    block: 'angular'
  },
  {
    id: 'rxjs',
    caption: 'RxJs',
    icon: 'rxjs.svg',
    categories: [],
    items: [],
    block: 'angular'
  },
  {
    id: 'react',
    caption: 'React',
    icon: 'react.svg',
    categories: [],
    items: []
  },
  {
    id: 'vue',
    caption: 'vue',
    icon: 'vue.svg',
    categories: [],
    items: [],
    block: 'vue'
  },
  {
    id: 'vueState',
    caption: 'Vue States',
    icon: 'pinia.svg',
    categories: [],
    items: [],
    block: 'vue'
  },
  {
    id: 'vuetify',
    caption: 'vuetify',
    icon: 'vuetify.svg',
    categories: [],
    items: [],
    block: 'vue'
  },
  {
    id: 'vueRoute',
    caption: 'Vue Router',
    icon: 'vue.svg',
    categories: [],
    items: [],
    block: 'vue'
  },
  {
    id: 'nuxt',
    caption: 'Nuxt.js',
    icon: 'nuxt.svg',
    categories: [],
    items: [],
    block: 'vue'
  },
  {
    id: 'php',
    caption: 'PHP',
    icon: 'php.svg',
    categories: [],
    items: [],
    block: 'back'
  },
  {
    id: 'server',
    caption: 'Server',
    icon: 'server.svg',
    categories: [],
    items: [],
    block: 'back'
  },
  {
    id: 'git',
    caption: 'Git',
    icon: 'git.svg',
    categories: [],
    items: []
  },
  {
    id: 'docker',
    caption: 'Docker',
    icon: 'docker.svg',
    categories: [],
    items: []
  },
  {
    id: 'npm',
    caption: 'npm',
    icon: 'npm.svg',
    categories: [],
    items: [],
    block: 'terminal'
  },
  {
    id: 'terminal',
    caption: 'Terminal',
    icon: 'terminal.svg',
    categories: [],
    items: [],
    block: 'terminal'
  },
  {
    id: 'gulp',
    caption: 'Gulp',
    icon: 'gulp.svg',
    categories: [],
    items: [],
    block: 'terminal'
  },
  {
    id: 'webpack',
    caption: 'Webpack',
    icon: 'webpack.svg',
    categories: [],
    items: [],
    block: 'terminal'
  },
  {
    id: 'tools',
    caption: 'Tools',
    icon: 'tools.svg',
    categories: [],
    items: [],
    block: 'terminal'
  },
  {
    id: 'ide',
    caption: 'IDE',
    icon: 'vscode.svg',
    categories: [],
    items: [],
    block: 'terminal'
  },
  {
    id: 'figma',
    caption: 'Figma',
    icon: 'figma.svg',
    categories: [],
    items: []
  },
  {
    id: 'book',
    caption: 'Book',
    icon: 'book.svg',
    categories: [],
    block: 'text',
    items: []
  },
  {
    id: 'english',
    caption: 'English',
    icon: 'translate.svg',
    categories: [],
    block: 'text',
    items: []
  },
  {
    id: 'help',
    caption: 'Help',
    icon: 'help.svg',
    categories: [],
    items: []
  }
];
