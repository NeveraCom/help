const category = {

	create(lib, id, caption) {
	  // add category
	  libraries.find(e => e.id === lib).categories.push({ id, caption });

		if (libs.current) {
			// update select with categories in contentSave
		  selectUpdate(libraries.find(e => e.id === libs.current), id);

			// update menu
			menu.displayLibsMenu();
		}
	},

	remove(categoryId) {
  	const indexLib = libraries.findIndex(e => e.id === libs.current);
  	const indexCat = libraries[indexLib].categories.findIndex(e => e.id === categoryId);

  	if (indexLib >= 0 && indexCat >= 0) {
  		const categoryName = libraries[indexLib].categories[indexCat].caption;

	  	if (window.confirm(`Delete category <${categoryName}>?`)) {
	  		libraries[indexLib].categories.splice(indexCat, 1);

	  		// update menu
		 		menu.displayLibsMenu();
	  	}
  	} else {
  		showToast('Category not found', 'error');
  	}
  	
	},
}
