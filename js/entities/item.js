const item = {

	create(lib, categoryId, id, caption) {
		// statuses - empty / process / base / ready

		// add item
	  libraries.find(e => e.id === lib).items.push({
			categoryId,	id, caption, labels:[], status: "empty", values: [{id: Date.now().toString(),type:"header",data:{text:"Title",level:5}}]
		});

		if (libs.current && libs.current === lib) {
			// update select with categories in contentSave
		  selectUpdate(libraries.find(e => e.id === libs.current), id);

			// update menu
			menu.displayLibsMenu();
		}
	},

	remove(id) {
		const items = libraries.find(l => l.id === libs.current).items;
  	const index = items.map(i => i.id).indexOf(id);

  	if (index >= 0) {
  		const name = items[index]?.caption;
	  	if (window.confirm(`Delete item <${name}>`)) {
	  		items.splice(index, 1);
	  	}

	  	// update menu
		  menu.displayLibsMenu();

		  // toast
		  showToast('Item removed from db', 'success')
  	}
	},
}