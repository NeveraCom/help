/**
 * select lib from lib list
 */
document.getElementById('libs').addEventListener('click', (e) => {
  const btn = e.target.closest('.btn');

  if (!btn) return;

  const lib = btn.getAttribute('data-lib');

  if (lib !== libs.current) {
    libs.selectLib(lib);
    menu.displayLibsMenu(lib);
    libs.current = lib;

    openMenu();
  }
});

// collapse group in menu
document.getElementById('menu-accordion').addEventListener('click', (e) => {
  const btn = e.target.closest('.accordion-button');
  const btnItem = e.target.closest('.btn');
  if (btn) {
    // 1 - collapse all header
    const list = Array.apply(null, document.getElementById('menu-accordion').querySelectorAll('.accordion-button'));
    list.forEach(e => e.classList.add('collapsed'));
    // 2 - hide all group
    const listBody = Array.apply(null, document.getElementById('menu-accordion').querySelectorAll('.accordion-collapse'));
    listBody.forEach(e => e.classList.remove('show'));
    // 3 - show selected group
    btn.classList.remove('collapsed');
    btn.parentElement.querySelector('.accordion-collapse').classList.toggle('show')
  }

  if (btnItem) {
    // remove previous selected
    document.querySelectorAll('#menu-accordion .btn').forEach(e => e.classList.remove('btn_selected'));
    // show item as selected
    btnItem.classList.add('btn_selected');

    showItem(btnItem.getAttribute('data-category'), btnItem.getAttribute('data-item'));
  }
});



