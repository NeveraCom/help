const libs = {
  current: '',

  getLibTemplate(lib) {
    return `
      <button class="btn" data-lib="${lib.id}" title="${lib.caption}">
        <img src="./images/icons/${lib.icon}" alt="angular">
      </button>
    `;
  },

  displayLibsButtons() {
    let str = '';
    let libres = [...libraries];
    const list = [];

    while (libres.length > 0) {
      const block = libres[0].block;

      if (!block) {
        list.push([libres[0]]);
        libres.shift();

      } else {
        list.push(libres.filter(l => l.block === block));
        libres = [...libres.filter(l => l.block !== block)];
      }
    }

    for (let lib of list) {
      const border = lib.length > 2 ? 'content__lib-group_large' : '';
      str += `<div class='content__lib-group ${border}'>`;
      for (let l of lib) {
        str += this.getLibTemplate(l)
      }
      str += `</div>`;
    }

    document.getElementById('libs').innerHTML = str;
  },

  selectLib(lib) {
    // remove selection from current lib
    if (libs.current) {
      const selectedLibElement = document.querySelector(`#libs .btn[data-lib=${libs.current}]`);
      selectedLibElement.classList.remove('btn-outline-primary');
    }

    // add selection to selected lib
    const libBtn = document.querySelector(`#libs .btn[data-lib=${lib}]`);
    libBtn.classList.add('btn-outline-primary');
  },
}
