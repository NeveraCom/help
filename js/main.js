// let currentCategory;
// let currentItem;
let editable = false;
let hideMenu = false;
let hideSave = false;

document.getElementById('toggleSave').addEventListener('click', (e) => {
  hideMenu = !hideMenu;
  document.querySelector('.content__save').classList.toggle('hidden');
  document.querySelector('.header__save').classList.toggle('hidden');
  document.getElementById('toggleSave').classList.toggle('rotated');
})

document.getElementById('toggleMenu').addEventListener('click', (e) => {
  hideMenu = !hideMenu;
  document.querySelector('.content__menu').classList.toggle('hidden');
  document.querySelector('.header__menu').classList.toggle('hidden');
  document.getElementById('toggleMenu').classList.toggle('rotated');
})

function openMenu() {
  hideMenu = false;
  document.querySelector('.content__menu').classList.remove('hidden');
  document.querySelector('.header__menu').classList.remove('hidden');
  document.getElementById('toggleMenu').classList.remove('rotated');
}

document.getElementById('isEditable').addEventListener('click', toggleEditable);
function toggleEditable() {
  editor.readOnly.toggle().then(readonly => {
    document.getElementById('editable-content').classList.toggle('hidden');
    autoHeightTextarea();
    editable = isEditable.checked;
  });
}

function disableEditable() {
  if (editable) {
    editor.readOnly.toggle().then(readonly => {
      document.getElementById('editable-content').classList.add('hidden');
      autoHeightTextarea();
      editable = false;
      isEditable.checked = false;
    });
  }
}

// SAVE ITEM IN BUFFER
const copyTextareaBtn = document.getElementById('copy-result');
copyTextareaBtn.addEventListener('click', function(event) {
  const category = document.getElementById('category-select');
  // const newCategory = document.getElementById('category-add');
  const oldItem = document.getElementById('item-select');
  const newItemId = document.getElementById('item-add');
  const newItemCaption = document.getElementById('item-add-caption');
  const itemLabels = document.getElementById('item-labels');

  const indexItemInSelect = Array.from(oldItem.options).findIndex(e => e.value === oldItem.value);

  const result = {
    categoryId: category.value,
    id: newItemId.value || oldItem.value,
    caption: newItemCaption.value || Array.apply(null, oldItem.options)[indexItemInSelect].text,
    labels: itemLabels.value.length ? itemLabels.value.split(',').map(e => e.trim()) : [],
    status: contentSave.getStatus(),
    values: []
  };

  editor.save().then(res => {
    result.values = res.blocks;

    const copyTextarea = document.getElementById('result');
    copyTextarea.innerText = JSON.stringify(result);
    copyTextarea.focus();
    copyTextarea.select();

    try {
      const successful = document.execCommand('copy');
      successful
        ? showToast('Data copied to clipboard', 'success')
        : showToast('Data not copied to clipboard', 'error');
    } catch (err) {
      showToast('Error in copy command', 'error');
    }
  });
});


// SHOW ITEM
function showItem(categoryId, itemId) {

  const lib = libraries.find(e => e.id === libs.current);
  const item = lib.items.find(e => e.id === itemId && e.categoryId === categoryId);
  editor.render({blocks: item.values}).then(() => autoHeightTextarea());

  if (editable) {
    disableEditable();
  }

  // update delect
  selectUpdate(lib, categoryId);

  const itemSelect = document.getElementById('item-select');
  const itemsOfCategories = lib.items.filter(e => e.categoryId === categoryId);
  for (let item of itemsOfCategories) {
    const option = document.createElement('option');
    option.innerHTML = item.caption;
    option.setAttribute('value', item.id);
    itemSelect.appendChild(option);
  }
  itemSelect.value = itemId;

  // position
  document.getElementById('position').innerText = lib.caption + ' > ' + lib.categories.find(e => e.id === categoryId).caption + ' > ' + item.caption;

  // labels
  const labels = document.getElementById('item-labels');
  if (item.labels.length) {
    labels.value = item.labels.toString();
  } else {
    labels.value = '';
  }

  // labels on top
  if (item.labels.length) {
    let str = '';
    for (let label of item.labels) {
      str += `<li class="content__position-item">${label}</li>`;
    }
    document.getElementById('labels').innerHTML = str;
  } else {
    document.getElementById('labels').innerHTML = '';
  }

  // state
  contentSave.setStatus(item.status);
}


function selectUpdate(lib, categoryId) {
  // add data to selects
  // 1 clear data
  const listCategories = document.getElementById('category-select').querySelectorAll('option');
  for (let item of listCategories) item.remove();

  const listItems = document.getElementById('item-select').querySelectorAll('option');
  for (let item of listItems) item.remove();

  // 2 add data
  const categorySelect = document.getElementById('category-select');
  for (let category of lib.categories) {
    const option = document.createElement('option');
    option.innerHTML = category.caption;
    option.setAttribute('value', category.id);
    categorySelect.appendChild(option);
  }
  categorySelect.value = categoryId;
}

/**
 * SEARCH
 */
const searchField = document.getElementById('search');
searchField.addEventListener('keyup', search);
const searchButton = document.getElementById('search-button');
searchButton.addEventListener('click', search);
const searchClear = document.getElementById('search-clear');
searchClear.addEventListener('click', clearSearch);

function search() {
  menu.displayLibsMenu(libs.current, searchField.value.trim().toLowerCase());
}

function clearSearch() {
  searchField.value = '';
  menu.displayLibsMenu(libs.current);
}

/**
 * TOAST
 */
const toast = document.getElementById('toast');

function showToast(message = 'Unknown message', state = 'success', time = 2500) {
  toast.classList.remove('bg-danger', 'bg-primary');
  toast.querySelector('.toast-body').innerText = message;
  switch (state) {
    case 'danger':
    case 'error':
      toast.classList.add('bg-danger');
      break;
    case 'primary':
    default:
      toast.classList.add('bg-primary');
      break;
  }
  toast.classList.add('show');
  setTimeout(() => toast.classList.remove('show'), time);
}

function autoHeightTextarea() {
  const ta = document.querySelector('.codex-editor').querySelectorAll('.ce-code__textarea');

  ta.forEach(e => {
    e.style.height = "1px";
    e.style.height = (20 + e.scrollHeight) + "px";
  })
}

document.addEventListener("DOMContentLoaded", function() {
  /**
   * RESIZE TEXTAREA
   */
  const globalEditor = document.querySelector('.codex-editor');
  globalEditor.addEventListener('keyup', (e) => {
    const textarea = e.target.closest('textarea');
    if (textarea) {
      textarea.style.height = "1px";
      textarea.style.height = (12 + textarea.scrollHeight) + "px";
    }
  });
});
