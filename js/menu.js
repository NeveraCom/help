const menu = {

  displayLibsMenu(libId = libs.current, searchWord = '') {
    if (!libId) return;

    let str = '';
    const lib = libraries.find(e => e.id === libId);

    // 1 - filter items by search
    const searchedItems = !searchWord
      ? lib.items
      : lib.items.filter(item => {
        return (item.caption.toLowerCase().indexOf(searchWord) >= 0
            ? true
            : item.labels.some(label => label.indexOf(searchWord) >= 0)
        )
      });

    // 2 - remove empty categories
    if (!searchWord) {
      for (let cat of lib.categories) {
        // start category
        str += `
            <div class="accordion-item">
              <button 
                class="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseOne"
                aria-expanded="false"
                aria-controls="flush-collapseOne"
              >
                ${cat.caption} (${lib.items.filter(item => item.categoryId === cat.id).length})
              </button>
              <div class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#menu-accordion">
                <div class="accordion-body">
          `;

        // content category
        const items = searchedItems.filter(e => e.categoryId === cat.id);
        for (let item of items) {
          str += `
            <button 
              class="btn btn__status" 
              data-category="${cat.id}"
              data-status="${item.status}"
              data-item="${item.id}"
            >
              ${item.caption}
            </button>`;
        }

        // end category
        str += `
                </div>
              </div>
            </div>
          `;
      }
    } else {
      // content category
      str += `<div class="accordion-body">`;

      for (let item of searchedItems) {
        str += `
          <button 
            class="btn" 
            data-category="${item.categoryId}" 
            data-item="${item.id}"
          >
            ${item.caption} (${lib.categories.find(e => e.id === item.categoryId).caption})
          </button>`;
      }

      str += `</div>`;
    }
    document.getElementById('menu-accordion').innerHTML = str;
  }
}
