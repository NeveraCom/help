// const commonDialog = new bootstrap.Modal('#modal-dialog', {
// })

const modal = {
	dialog: new bootstrap.Modal('#modal-dialog'),

	modalWindow: document.getElementById('addCategoryModal'),
  modalBackdrop: document.querySelector('.modal-backdrop'),
  modalHeader: document.getElementById('modal-header'),
  fieldCaption: document.getElementById('new-category-name'),
  fieldId: document.getElementById('new-category-id'),
  selectLibs: document.getElementById('modal-select-libs'),
  selectCategories: document.getElementById('modal-select-categories'),
  blockCategories: document.getElementById('modal-block-categories'),

  entity: null,

  /**
   * @param (entity) string values: item | category
   */
  show(entity) {
  	this.entity = entity;
  	this.updateHeader(entity);
  	this.updateLibsList();
	  
	  if (entity === 'item') this.showCategoriesList(); else this.hideCategoriesList();
	  this.fieldCaption.value = '';
	  this.fieldCaption.classList.remove('is-invalid');
		this.fieldId.value = '';
		this.fieldId.classList.remove('is-invalid');

		this.dialog.show();
  },

  updateCategoriesList() {
  	const categories = this.selectLibs.value ? libraries.find(e => e.id === this.selectLibs.value).categories : libraries[0].categories;

  	// clear
  	const listCategories = this.selectCategories.querySelectorAll('option');
  	for (let item of listCategories) item.remove();

  	// add
  	for (let cat of categories) {
	    const option = document.createElement('option');
	    option.innerHTML = cat.caption;
	    option.setAttribute('value', cat.id);
	    this.selectCategories.appendChild(option);
	  }

	  // select current
	  this.selectCategories.value = categories[0].id;
  },

  hideCategoriesList() {
  	this.blockCategories.style.visibility = 'hidden';
  },

  showCategoriesList() {
  	this.blockCategories.style.visibility = 'visible';
  	this.updateCategoriesList();
  },

  updateLibsList() {
  	// clear
  	const listLibs = this.selectLibs.querySelectorAll('option');
  	for (let item of listLibs) item.remove();

  	// add
  	for (let lib of libraries) {
	    const option = document.createElement('option');
	    option.innerHTML = lib.caption;
	    option.setAttribute('value', lib.id);
	    this.selectLibs.appendChild(option);
	  }
	  this.selectLibs.value = libs.current || libraries[0].id;
  },

  updateHeader(entity) {
  	let caption = '';
  	switch (entity) {
	  	case 'item': caption = 'Add new item'; break;
	  	case 'category': caption = 'Add new category'; break;
	  	default: caption = 'Header';
  	}
  	this.modalHeader.innerText = caption;
  },

	approve() {
		// caption & id
		if (!this.isCaptionValid()) return;
		if (!this.isIdValid()) return;

		switch (this.entity) {
			case 'item': 
				item.create(this.selectLibs.value, this.selectCategories.value, this.fieldId.value, this.fieldCaption.value);
				this.dialog.hide();
				break;
			case 'category': 
				category.create(this.selectLibs.value, this.fieldId.value, this.fieldCaption.value);
				this.dialog.hide();
				break;
		}
	},

	isIdValid() {
		if (!this.fieldId.value.trim() || this.isEntityExist()) {
			this.fieldId.classList.add('is-invalid');
			return false;
		} else {
			this.fieldId.classList.remove('is-invalid');
			return true;
		}
	},

	isCaptionValid() {
		if (!this.fieldCaption.value.trim()) {
			this.fieldCaption.classList.add('is-invalid');
			return false;
		} else {
			this.fieldCaption.classList.remove('is-invalid');
			return true;
		}
	},

	isEntityExist() {
		let index;
		switch (this.entity) {
			case 'item': 
				index = libraries
									.find(l => l.id === this.selectLibs.value)
									.items.filter(c => c => c.id === this.selectCategories.value)
									.findIndex(i => i.id === this.fieldId.value);
				break;
			case 'category': 
				index = libraries
									.find(l => l.id === this.selectLibs.value)
									.categories.findIndex(c => c.id === this.fieldId.value);
				break;
		}

		return index < 0 ? false : true;
	},

	close() {
		this.modalWindow.classList.remove('show');
		this.modalBackdrop.classList.remove('show');
	},
}


// document.getElementById('modal-btn-apply').addEventListener('click', modal.approve());




// const modalButtonApply = document.getElementById('modal-btn-apply');
// modalButtonApply.addEventListener('click', modal.apply());