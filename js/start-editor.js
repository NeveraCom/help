document.addEventListener("DOMContentLoaded", function(){
  editor = new EditorJS({
    holder: 'editorjs',
    minHeight: '100px',
    readOnly: true,
    tools: {
      header: {
        class: Header,
        placeholder: 'Enter a header',
        levels: [2, 3, 4],
        defaultLevel: 3
      },
      table: {
        class: Table,
        inlineToolbar: true,
        config: {
          rows: 2,
          cols: 3,
        },
      },
      code: CodeTool,
      // linkTool: {
      //   class: LinkTool,
      //   config: {
      //     endpoint: 'http://localhost:8008/fetchUrl', // Your backend endpoint for url data fetching,
      //   }
      // },
      list: {
        class: NestedList,
        inlineToolbar: true,
      },
      image: { // just
        class: SimpleImage,
        inlineToolbar: true,
        config: {
          placeholder: 'Paste image URL'
        }
      },
      delimiter: Delimiter,
      // image: {
      // 	class: ImageTool,
      // 	config: {
      // 		endpoints: {
      // 			// byFile: '/images', // Your backend file uploader endpoint
      // 			byUrl: '/images', // Your endpoint that provides uploading by Url
      // 		}
      // 	}
      // },
      // list: {
      // 	class: List,
      // 	inlineToolbar: true,
      // 	config: {
      // 		defaultStyle: 'unordered'
      // 	}
      // },
    },
  });
});


